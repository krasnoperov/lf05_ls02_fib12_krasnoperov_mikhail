import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInprozent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private String treffer;
	private ArrayList<String> broadcastKommunikator =
			new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis =
			new ArrayList<Ladung>();
	
	public Raumschiff(){
		
	}
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, 
						int schildeInprozent, int huelleInProzent,
						int lebenserhaltungssystemeInProzent,
						int androidenAnzahl, String schiffsname) {
				this.photonentorpedoAnzahl = photonentorpedoAnzahl;
				this.energieversorgungInProzent = energieversorgungInProzent;
				this.schildeInprozent = schildeInprozent;
				this.huelleInProzent = huelleInProzent;
				this.androidenAnzahl = androidenAnzahl;
				this.schiffsname = schiffsname;
		
	}
//Source -> g�generate fields and constructors 
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInprozent() {
		return schildeInprozent;
	}
	public void setSchildeInprozent(int schildeInProzent) {
		this.schildeInprozent = schildeInprozent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	public void addLadung (Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	public void zustandRaumschiff() {
		System.out.println("Zustand des Raumschiff:" + schiffsname);
		System.out.println("Photonentorpedoanzahl:" + photonentorpedoAnzahl);
		System.out.println("Schilde in Prozent:" + schildeInprozent);
		System.out.println("H�lle in Prozent:" + huelleInProzent);
		System.out.println("Lebenserhaltungssysteme in Prozent:" + lebenserhaltungssystemeInProzent);
		System.out.println("Androidenanzahl:" + androidenAnzahl);
	}
	/** 
	 * Der Zustand des Raumschiffes wird ausgegeben: 
	 * - Photonentorpedoanzahl
	 * - Schilde in Prozent
	 * - H�lle in Prozent
	 * - Lebenserhaltungssysteme
	 * - Androidenanzahl
	 * @param r
	 */
	public void ladungsVerzeichnis() {
		for (Ladung ladung : ladungsverzeichnis) {
			System.out.println(ladung);
		}
	}
	/** 
	 * Hier wird der Ladungsverzeichnis ausgegeben.
	 * @param r
	 */
	
	public void photonentorpedoSchiessen (Raumschiff r) {

		boolean photonentorpedoSchiessen;
		if (photonentorpedoSchiessen = true) {
			
			photonentorpedoAnzahl -= 1;
		
		    System.out.println("Photonentorpedo abgeschossen");	
		    treffer(r);
		}
	else if (photonentorpedoAnzahl == 0) {
		
		System.out.println("-=*Click*=- ");
		System.out.println("Keine Photonentorpedos gefunden!");
		}
	
		
	}
	/**
	 * Mit dieser Klasse werden Photonen abgeschossen. 
	 * Wenn die Klasse angewendet wird, wird die Anzahl der Photonentorpedos um 1 reduziert
	 * und eine Nachricht an alle geschickt, dass das Photonentorpedo abgeschossen wurde.
	 * Falls ein Schiff getroffen wird, soll die Methode Treffer aufgerufen, 
	 * @param r
	 */
	
	public void treffer(Raumschiff r) {
	 System.out.println(schiffsname +" wurde getroffen!");
	}
	/** 
	 * Hiermit wird die Nachricht mit dem Namen des Schiffes und Nachricht "wurde getroffen!" ausgegeben.
	 * Diese Methode wird bei phaserkanonenSchiessen und photonentorpedosSchiessen angewendet.
	 * @param r
	 */
	
	public void phaserkanonenSchiessen (Raumschiff r)
	{
		if(energieversorgungInProzent < 50 ) {
		System.out.println("-=*Click*=- ");	
		}
		else 
		{ 
			energieversorgungInProzent -= 50;
			System.out.println("Phaserkanone abgeschossen");
			treffer(r);
		}
	}
	
	/**
	 * Hiermit werden Phaserkanonen abschie�en aktiviert.
	 * Wenn die Energieversorgung des Raumschiffes kleiner als 50 ist,
	 * wird die Nachricht "click" ausgegeben. Am sonsten wird die Energieversorgung um 50% reduziert.
	 * 
	 * @param message
	 */
	public void nachrichtAnAlle (String message)
	{
		System.out.println(message);
		broadcastKommunikator.add(message);
	}
	/** 
	 * Mit dieser Methode wird eine Nachricht an alle Schiffe ausgegeben.
	 * Bei message soll die jeweilige Nachricht angegeben werden.
	 * @param r
	 */
	public void logBuch() {
		System.out.println("Logbucheintr�ge von " + schiffsname);
		for(String eintrag: this.broadcastKommunikator) {
			System.out.println(eintrag);
		}
	}
	/** 
	 * Damit werden alle Logbucheintr�ge von einem der ausgew�hlten Raumschiffen 
	 * ausgegeben.
	 * @param r
	 */
	

	public void schiffReparatur(Raumschiff r) {
		
	}
	@Override
	public String toString() {
		return "Raumschiff [photonentorpedoAnzahl=" + photonentorpedoAnzahl + ", energieversorgungInProzent="
				+ energieversorgungInProzent + ", schildeInprozent=" + schildeInprozent + ", huelleInProzent="
				+ huelleInProzent + ", lebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent
				+ ", androidenAnzahl=" + androidenAnzahl + ", schiffsname=" + schiffsname + ", treffer=" + treffer
				+ ", broadcastKommunikator=" + broadcastKommunikator + ", ladungsverzeichnis=" + ladungsverzeichnis
				+ "]";
	}
	
	
	
	// source getters and setters




}
