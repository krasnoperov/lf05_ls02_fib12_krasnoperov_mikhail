import java.util.ArrayList;
public class Auto1 {
	private String marke;
	private String farbe;
	private ArrayList<Ladung> ladungen =
			new ArrayList<Ladung>();
	public Auto1() {
	}
	public String getMarke() {
		return marke;
	}
	public void setMarke(String marke) {
		this.marke = marke;
	}
	public String getFarbe() {
		return farbe;
	}
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	public void addLadung (Ladung ladung) {
		ladungen.add(ladung);
	}
}
