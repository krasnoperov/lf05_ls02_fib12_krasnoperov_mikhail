
public class RaumschiffTest {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff();
		Raumschiff romulaner = new Raumschiff();
		Raumschiff vulkanier = new Raumschiff();
		Ladung lad1 = new Ladung();
		Ladung lad2 = new Ladung();
		Ladung lad3 = new Ladung();
		Ladung lad4 = new Ladung();
		Ladung lad5 = new Ladung();
		Ladung lad6 = new Ladung();
		Ladung lad7 = new Ladung();
		
		
		lad1.setBezeichnung("Ferengi Schneckensaft");
		lad1.setMenge(200);
		
		lad2.setBezeichnung("Borg-Schrott");
		lad2.setMenge(5);
		
		lad3.setBezeichnung("Rote Materie");
		lad3.setMenge(2);
		
		lad4.setBezeichnung("Forschingssonde");
		lad4.setMenge(35);
		
		lad5.setBezeichnung("Bat'leth Klingonen Schwert");
		lad5.setMenge(200);
		
		lad6.setBezeichnung("Plasma-Waffe");
		lad6.setMenge(50);
		
		lad7.setBezeichnung("Photonentorpedo");
		lad7.setMenge(3);
		
		
		klingonen.setPhotonentorpedoAnzahl(1);
		klingonen.setEnergieversorgungInProzent(100);
		klingonen.setSchildeInprozent(100);
		klingonen.setHuelleInProzent(100);
		klingonen.setSchiffsname("IKS Hegh'ta");
		klingonen.setAndroidenAnzahl(2);
		klingonen.addLadung(lad1);
		klingonen.addLadung(lad5);
		
		romulaner.setPhotonentorpedoAnzahl(2);
		romulaner.setEnergieversorgungInProzent(100);
		romulaner.setSchildeInprozent(100);
		romulaner.setHuelleInProzent(100);
		romulaner.setLebenserhaltungssystemeInProzent(100);
		romulaner.setSchiffsname("IRW Khazara");
		romulaner.setAndroidenAnzahl(2);
		romulaner.addLadung(lad2);
		romulaner.addLadung(lad3);
		romulaner.addLadung(lad6);

		vulkanier.setPhotonentorpedoAnzahl(0);
		vulkanier.setEnergieversorgungInProzent(80);
		vulkanier.setSchildeInprozent(80);
		vulkanier.setHuelleInProzent(50);
		vulkanier.setLebenserhaltungssystemeInProzent(100);
		vulkanier.setSchiffsname("Ni'Var");
		vulkanier.setAndroidenAnzahl(5);
		vulkanier.addLadung(lad4);
		vulkanier.addLadung(lad7);
		
		System.out.println("Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.");
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.println("Die Romulaner schie�en mit der Phaserkanone zur�ck.");
		romulaner.phaserkanonenSchiessen(klingonen);
		System.out.println("Die Vulkanier senden eine Nachricht an Alle:");
		vulkanier.nachrichtAnAlle("'Gewalt ist nicht logisch!'");
		System.out.println("Die Klingonen rufen den Zustand Ihres Raumschiffes ab");
		klingonen.zustandRaumschiff();
		System.out.println("Klingonen geben Ihr Ladungsverzeichnis aus:");
		klingonen.ladungsVerzeichnis();
		System.out.println("Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner:");
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.println("Die Klingonen rufen den Zustand Ihres Raumschiffes ab:");
		klingonen.zustandRaumschiff();
		System.out.println("Die Klingonen geben Ihr Ladungsverzeichnis aus:");
		klingonen.ladungsVerzeichnis();
		System.out.println("Die Romulaner rufen den Zustand Ihres Raumschiffes ab:");
		romulaner.zustandRaumschiff();
		System.out.println("Die Romulaner geben Ihr Ladungsverzeichnis aus:");
		romulaner.ladungsVerzeichnis();
		System.out.println("Die Vulkanier rufen den Zustand Ihres Raumschiffes ab:");
		vulkanier.zustandRaumschiff();
		System.out.println("Die Vulkanier geben Ihr Ladungsverzeichnis aus:");
		vulkanier.ladungsVerzeichnis();
//		klingonen.logBuch();
//		vulkanier.logBuch();
		
	}

}
